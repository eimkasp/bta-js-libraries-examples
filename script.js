$(document).ready(function(){
    
    // Slick slider
    $('.your-class').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
      //  autoplay: true,
     //   autoplaySpeed: 10,
        arrows: false,
        dots: true
    });


    // Isotope
    
    var filtravimas = $('.grid').isotope({
        // options
        itemSelector: '.grid-item',
        layoutMode: 'fitRows',
     
      });


      $(".pasirinkimas").on('click', function() {
        // Gaunu paspausto elemento id
        console.log($(this).attr("id"));
            // $(this).data('filter');
        var elementFilter = $(this).attr("data-filter");

        console.log(elementFilter);

        // Filtravimas pagal klases pavadinima
        filtravimas.isotope({ 
            filter: elementFilter
        });
      });
});